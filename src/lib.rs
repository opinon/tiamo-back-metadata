#![feature(
    proc_macro_hygiene,
    decl_macro,
    custom_attribute,
    type_alias_enum_variants,
    bind_by_move_pattern_guards
)]

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate rocket_contrib;

pub mod db;
pub mod models;
pub mod schema;
pub mod user_auth;
