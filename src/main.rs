#![feature(
    proc_macro_hygiene,
    decl_macro,
    custom_attribute,
    type_alias_enum_variants,
    bind_by_move_pattern_guards
)]

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate rocket_contrib;

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate serde_derive;

pub mod api;
pub mod user_api;

use rocket::Request;
use rocket_contrib::json::{Json, JsonValue};
use tiamo_back_metadata::db::DatabaseConn;

#[get("/")]
/// Route permettant de vérifier le status du server
fn index() -> &'static str {
    "TIAMO Back-End System"
}

#[catch(404)]
/// Catcher pour les requêtes qui se terminent en 404
fn not_found(req: &Request) -> Json<JsonValue> {
    Json(json!({"status": "404 not found"}))
}

/// Point d'entrée du programme
fn main() {
    rocket::ignite()
        .attach(DatabaseConn::fairing())
        .mount("/", routes![index])
        .mount(
            "/user",
            routes![
                self::user_api::get_user_token,
                self::user_api::new_user,
                self::user_api::user_profile,
                self::user_api::user_by_id,
                self::user_api::user_list_friends,
                self::user_api::befriend_user,
                self::user_api::modif_user
            ],
        )
        .mount(
            "/music",
            routes![
                self::api::music::music_by_id,
                self::api::music::music_page,
                self::api::music::music_update,
                self::api::music::music_insert,
                self::api::music::music_search,
                self::api::music::music_like,
                self::api::music::music_unlike,
            ],
        )
        .mount(
            "/album",
            routes![
                self::api::album::album_by_id,
                self::api::album::album_insert,
                self::api::album::album_update,
                self::api::album::album_like
            ],
        )
        .mount(
            "/artiste",
            routes![
                self::api::artiste::artiste_by_id,
                self::api::artiste::artiste_insert,
                self::api::artiste::artiste_modif,
                self::api::artiste::artiste_like,
            ],
        )
        .register(catchers![not_found])
        .launch();
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_it_works() {
        println!("Works!");
    }
}
