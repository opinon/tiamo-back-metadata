use crate::api::APIRef;
use rocket::http::Status;
use rocket::request::FromRequest;
use rocket::request::Outcome;
use rocket::Request;
use rocket_contrib::json::{Json, JsonValue};

use tiamo_back_metadata::db::DatabaseConn;
use tiamo_back_metadata::models::{InsertToken, InsertUser, Token};
use tiamo_back_metadata::schema::{friend_with, token, user, musique_like, musique};
use tiamo_back_metadata::user_auth::UserAuth;

use chrono::{NaiveDateTime, Utc};
use rand::{thread_rng, Rng};
use std::collections::HashMap;
use std::error::Error;

use diesel::prelude::*;

/// Représente une combinaise de mot de passe et de nom d'utilisateur
pub struct UserPassCombination {
    /// Nom de l'utilisateur
    libelle_user: String,
    /// Mot de passe
    password: String,
}

/// Implémentation du trait "UserPassCombination". Permet à la structure d'agir
/// comme un garde de requête : si les informations ne sont pas rentrées
/// ou qu'elles ne sont pas cohérentes (user ou pass vides, par example), alors
/// la requête n'aboutira pas.
impl<'a, 'r> FromRequest<'a, 'r> for UserPassCombination {
    type Error = &'a str;
    /// Résoud la combinaison via la requête Rocket
    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        let headers = request.headers();
        let user: String = headers.get_one("User").unwrap_or("").into();
        let pass: String = headers.get_one("Pass").unwrap_or("").into();

        if user != "" && pass != "" {
            Outcome::Success(UserPassCombination {
                libelle_user: user,
                password: pass,
            })
        } else {
            Outcome::Failure((Status::BadRequest, "Incorrect user information"))
        }
    }
}

/// Représente un utilisateur dans la base de données
#[derive(Identifiable, Queryable, PartialEq, Debug)]
#[table_name = "user"]
pub struct User {
    /// Identifiant de l'utilisateur
    pub id: i32,
    /// Identifiant du token unique associé à l'utilisateur
    pub id_token: Option<i32>,
    /// Date de création de l'utilisateur
    pub date_creation: NaiveDateTime,
    /// Libelle de l'utilisateur (son pseudo)
    pub libelle_user: String,
    /// Mot de passe de l'utilisateur
    pub mdp_user: String,
}


impl APIUser {
    /// Crée un APIUser à partir d'un `User` de la base de données et d'une liste
    /// d' `APIRef` vers les musiques
    pub fn new(user: User, musics: Vec<APIRef>) -> Self {
        APIUser {
            date_creation: user.date_creation.format("%d-%m-%Y %H:%M:%S").to_string(),
            libelle_user: user.libelle_user,
            musics_liked: musics,
        }
    }
}

/// Génère un token (String aléatoire de 20 caractères)
fn gen_token() -> String {
    thread_rng().gen_ascii_chars().take(20).collect()
}

#[get("/token")]
/// Crée un nouveau token pour l'utilisateur concerné par la combinaison
/// User/Pass spécifiée. Retourne une erreur 400 si aucun utilisateur ne
/// correspond ou en cas de problème de connection à la base de données
pub fn get_user_token(user_pass: UserPassCombination, db: DatabaseConn) -> Option<Json<JsonValue>> {
    let res = user::table
        .filter(user::libelle_user.eq(user_pass.libelle_user))
        .filter(user::mdp_user.eq(user_pass.password))
        .inner_join(token::table)
        .select(token::id_token)
        .load::<i32>(&db.0)
        .ok();

    match res {
        Some(tok) if tok.len() == 1 => {
            let new_token = gen_token();
            let datetime_now: NaiveDateTime = Utc::now().naive_local();
            let target = token::table.filter(token::id_token.eq(tok[0]));
            let res_update = diesel::update(target)
                .set((
                    token::content.eq(new_token.clone()),
                    token::date_creation.eq(datetime_now),
                ))
                .execute(&db.0);

            if res_update.is_ok() {
                Some(Json(json!({ "token": new_token })))
            } else {
                None
            }
        }
        _ => None,
    }
}

#[post("/")]
/// Crée un nouvel utilisateur dans la base de données. Retourne un JSON:
/// * status == 'error' | 'success' si l'insertion a réussi ou échoué
/// * <Optionel> reason = raison de l'erreur en base
/// * <Optionel> token = token de l'utilisateur créé
pub fn new_user(user_pass: UserPassCombination, db: DatabaseConn) -> Json<JsonValue> {
    let existing_user = user::table
        .filter(user::libelle_user.eq(&user_pass.libelle_user))
        .load::<User>(&db.0)
        .ok();

    match existing_user {
        Some(users) if users.len() == 1 => {
            // Il y a déjà un utilisateur! pas d'insertion.
            Json(json!({
                "status": "error",
                "reason": "Il existe déjà un utilisateur"
            }))
        }
        _ => {
            let token_to_insert = InsertToken {
                content: gen_token(),
                date_creation: Utc::now().naive_local(),
                validity_period_days: 1,
            };

            let res_insert_token = diesel::insert_into(token::table)
                .values(&token_to_insert)
                .execute(&db.0);

            if let Err(e) = res_insert_token {
                return Json(
                    json!({"status": "error", "reason": format!("Erreur lors de la création du token: {}", e.description())}),
                );
            }

            let new_token: Token = token::table
                .order(token::id_token.desc())
                .first(&db.0)
                .unwrap();

            let user_to_insert = InsertUser {
                id_token: Some(new_token.id_token),
                date_creation: Utc::now().naive_local(),
                libelle_user: user_pass.libelle_user,
                mdp_user: user_pass.password,
            };
            let res_insert = diesel::insert_into(user::table)
                .values(&user_to_insert)
                .execute(&db.0);

            if res_insert.is_ok() {
                Json(json!({
                    "status": "success",
                    "token": new_token.content,
                }))
            } else {
                Json(json!({
                    "status": "error",
                    "reason": "Erreur lors de la création de l'utilisateur",
                }))
            }
        }
    }
}

/// Représente un utilisateur dans la base de données au format JSON
#[derive(Serialize, Debug)]
pub struct APIUser {
    /// Date de création de l'utilisateur au format "JOUR-MOIS-ANNEE HEURE:MINUTE"
    pub date_creation: String,
    /// Libelle de l'utilisateur (son pseudo)
    pub libelle_user: String,
    /// Musiques que l'utilisateur a aimé
    pub musics_liked: Vec<APIRef>,
}

#[get("/profile")]
/// Récupère le profil de l'utilisateur identifié par le Token
pub fn user_profile(user_auth: UserAuth, db: DatabaseConn) -> Json<APIUser> {
    let id = user_auth.user_id;
    user_by_id(user_auth, id, db).unwrap()
}

#[get("/profile/<id>")]
/// Récupère le profil de l'utilisateur dont l'ID est spécifié dans l'URL
pub fn user_by_id(_user_auth: UserAuth, id: i32, db: DatabaseConn) -> Option<Json<APIUser>> {
    let res_music = musique_like::table
        .filter(musique_like::id_user.eq(id))
        .inner_join(musique::table)
        .select((musique::libelle_musique, musique::id_musique))
        .load::<APIRef>(&db.0)
        .ok();

    if let Some(musics) = res_music {
    return user::table
        .filter(user::id_user.eq(id))
        .first::<User>(&db.0)
        .ok()
        .map(|u| Json(APIUser::new(u, musics)));
    } else {
        None
    }
}

#[get("/friends")]
/// Récupère une liste sous format JSON de références d'API qui représente les amis 
/// (personnes suivies) par l'utilisateur identifié par le Token 
pub fn user_list_friends(user_auth: UserAuth, db: DatabaseConn) -> Option<Json<Vec<APIRef>>> {
    let friends = friend_with::table
        .filter(friend_with::id_user_2.eq(user_auth.user_id))
        .inner_join(user::table)
        .select(user::all_columns)
        .load::<User>(&db.0);

    if let Ok(f) = friends {
        let refs: Vec<APIRef> = f
            .into_iter()
            .map(|user| (user.id, user.libelle_user))
            .collect::<HashMap<i32, String>>()
            .into_iter()
            .map(|(id, user)| APIRef {
                id: Some(id),
                libelle: user,
            })
            .collect();

        return Some(Json(refs));
    }
    None
}

#[post("/befriend/<libelle>")]
/// Ajoute une relation d'amitié entre deux utilisateurs
/// (l'utilisateur identifié suit l'utilisateur dont le pseudo est passé en paramètre)
pub fn befriend_user(
    user_auth: UserAuth,
    libelle: String,
    db: DatabaseConn,
) -> Result<String, Status> {
    let user = user::table
        .filter(user::libelle_user.eq(libelle))
        .first::<User>(&db.0);

    if user.is_err() {
        return Err(Status::NotFound);
    }
    let user = user.unwrap();
    let insert_friendship = InsertFriendship::new(user.id, user_auth.user_id);

    if diesel::insert_into(friend_with::table)
        .values(&insert_friendship)
        .execute(&db.0)
        .is_err()
    {
        Err(Status::BadRequest)
    } else {
        Ok(user.libelle_user)
    }
}

#[derive(Insertable)]
#[table_name = "friend_with"]
/// Structure stockant une relation de Follow entre deux utilisateurs.
/// L'utilisateur 2 suit l'utilisateur 1.
pub struct InsertFriendship {
    /// ID de l'utilisateur suivi
    id_user_1: i32,
    /// ID de l'utilisateur qui suit l'autre
    id_user_2: i32,
    /// Date à laquelle le suivi a été déclaré
    date_association: NaiveDateTime,
}

impl InsertFriendship {
    /// Crée une nouvelle instance d'InsertFriendship
    pub fn new(followed: i32, follower: i32) -> Self {
        InsertFriendship {
            id_user_1: followed,
            id_user_2: follower,
            date_association: Utc::now().naive_local(),
        }
    }
}

/// Implémentation du comportement permettant à un utilisateur stocké en base de données
/// d'être transformé en référence d'API
impl Into<APIRef> for User {
    /// Effectue la transformation
    fn into(self) -> APIRef {
        APIRef {
            libelle: self.libelle_user,
            id: Some(self.id),
        }
    }
}

#[derive(Deserialize)]
/// Structure deserializable représentant un user et un password
pub struct JsonUserPass {
    /// Nom d'utilisateur
    user: String,
    /// Mot de passe
    pass: String,
}

#[derive(AsChangeset)]
#[table_name = "user"]
/// Structure permettant d'appliquer des changements (update) sur la table User 
/// de la base de données
pub struct UserChangeset {
    /// ID de l'utilisateur à modifier
    id_user: i32,
    /// Nouveau libellé de l'utilisateur
    libelle_user: String,
    /// Nouveau mot de passe de l'utilisateur
    mdp_user: String,
}

/// Comportement d'un UserChangeset
impl UserChangeset {
    /// Crée une nouvelle instance à partir de l'ID à modifier et des nouvelles
    /// informations d'utilisateur
    pub fn new(user_pass: JsonUserPass, id: i32) -> Self {
        UserChangeset {
            id_user: id,
            libelle_user: user_pass.user,
            mdp_user: user_pass.pass,
        }
    }
}

#[put("/", data = "<new_user>")]
/// Route permettant la modification d'un utilisateur suivant les informations
/// passées dans le JSON contenu dans le body
pub fn modif_user(
    user_auth: UserAuth,
    new_user: Json<JsonUserPass>,
    db: DatabaseConn,
) -> Option<Json<APIUser>> {
    let changeset = UserChangeset::new(new_user.into_inner(), user_auth.user_id);
    if diesel::update(user::table)
        .set(&changeset)
        .execute(&db.0)
        .is_err()
    {
        None
    } else {
        Some(user_profile(user_auth, db))
    }
}
