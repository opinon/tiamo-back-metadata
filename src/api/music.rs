use crate::api::APIRef;
use tiamo_back_metadata::db::DatabaseConn;
use tiamo_back_metadata::schema::{album, artiste, genre, musique, musique_genre, musique_like};
use tiamo_back_metadata::user_auth::UserAuth;

use diesel::prelude::*;

use rocket_contrib::json::{Json, JsonValue};

#[derive(Serialize, Debug)]
/// Structure de données représentant une musique distribuée par l'API
pub struct APIMusic {
    /// Titre de la musique
    titre: String,
    /// Référence d'API vers l'Artiste
    artiste: Option<APIRef>,
    /// Référence d'API vers l'Album
    album: Option<APIRef>,
    /// Référence d'API vers les genres
    genres: Vec<APIRef>,
}

/// Structure de données permettant de stocker les changements en base
/// sur les musiques
#[derive(Insertable, AsChangeset, Deserialize, Identifiable)]
#[table_name = "musique"]
#[primary_key("id_musique")]
pub struct InsertMusic {
    /// ID en base de la musique
    id_musique: Option<i32>,
    /// Titre de la musique
    libelle_musique: String,
    /// URL de la miniature (cover de l'album)
    miniature_path: Option<String>,
    /// ID de l'artiste
    id_artiste: Option<i32>,
    /// ID de l'album
    id_album: Option<i32>,
}

/// Implémentation d'un CRUD pour les musiques
impl APIMusic {
    /// Lit une musique depuis la base de données, avec son ID spécifiée en paramètre
    pub fn read(id: i32, db: &DatabaseConn) -> Option<APIMusic> {
        let musique = musique::table
            .left_join(artiste::table)
            .left_join(album::table)
            .filter(musique::id_musique.eq(id))
            .select((
                musique::libelle_musique,
                (artiste::nom_artiste, artiste::id_artiste).nullable(),
                (album::nom_album, album::id_album).nullable(),
            ))
            .load::<(String, Option<APIRef>, Option<APIRef>)>(&db.0);

        // Awful hack to get first music
        if musique.is_err() {
            return None;
        }

        let musique = musique.unwrap();

        if musique.len() != 1 {
            return None;
        }

        let musique = musique[0].clone();

        let genres = musique_genre::table
            .filter(musique_genre::id_musique.eq(id))
            .inner_join(genre::table)
            .select((genre::libelle_genre, genre::id_genre))
            .load::<APIRef>(&db.0);

        if genres.is_err() {
            None
        } else {
            let genres = genres.unwrap();
            Some(APIMusic {
                titre: musique.0,
                artiste: musique.1,
                album: musique.2,
                genres,
            })
        }
    }

    /// Lit toutes les musiques présentes sur la page dont l'identifiant est donné
    /// (une page = 20 musiques)
    pub fn read_page(page: i32, db: &DatabaseConn) -> Vec<APIMusic> {
        (page * 20..(page + 1) * 20)
            .filter_map(|i| APIMusic::read(i, db))
            .collect()
    }

    /// Crée une musique en base de données, dont le libellé est donné en paramètre
    pub fn create(libelle: String, db: &DatabaseConn) -> bool {
        let insert_musique = InsertMusic {
            id_musique: None,
            libelle_musique: libelle,
            miniature_path: None,
            id_artiste: None,
            id_album: None,
        };

        let res = diesel::insert_into(musique::table)
            .values(&insert_musique)
            .execute(&db.0);

        res.is_ok()
    }

    /// Effectue une mise à jour d'un élément APIMusic dans la base
    pub fn update(updated: InsertMusic, db: &DatabaseConn) -> bool {
        diesel::update(musique::table)
            .set(&updated)
            .execute(&db.0)
            .is_ok()
    }
}

#[get("/<id>")]
/// Route permettant de récupérer les informations d'une musique par ID
pub fn music_by_id(_user_auth: UserAuth, id: i32, db: DatabaseConn) -> Option<Json<APIMusic>> {
    let music = APIMusic::read(id, &db);
    music.map(|m| Json(m))
}

#[get("/page/<id_page>")]
/// Route permettant de récupérer les informations de musique par page.
/// Une page = 20 musiques
pub fn music_page(_user_auth: UserAuth, id_page: i32, db: DatabaseConn) -> Json<Vec<APIMusic>> {
    let musics = APIMusic::read_page(id_page, &db);
    Json(musics)
}

#[post("/<libelle>")]
/// Route permettant l'insertion d'une nouvelle musique avec le nom "libelle"
pub fn music_insert(
    _user_auth: UserAuth,
    libelle: String,
    db: DatabaseConn,
) -> Option<Json<APIMusic>> {
    if APIMusic::create(libelle, &db) {
        let musique = musique::table
            .order(musique::id_musique.desc())
            .select(musique::id_musique)
            .first::<Option<i32>>(&db.0)
            .unwrap()
            .unwrap();
        let musique = APIMusic::read(musique, &db).unwrap();
        Some(Json(musique))
    } else {
        None
    }
}

#[put("/<id>", data = "<music>")]
/// Route permettant la modification d'une musique. Suivant les données JSON de la musique.
pub fn music_update(
    _user_auth: UserAuth,
    id: i32,
    mut music: Json<InsertMusic>,
    db: DatabaseConn,
) -> Option<Json<APIMusic>> {
    music.id_musique = Some(id);
    if APIMusic::update(music.into_inner(), &db) {
        Some(Json(APIMusic::read(id, &db).unwrap()))
    } else {
        None
    }
}

#[get("/search/<libelle>")]
/// Route permettant la recherche d'une musique par libelle
pub fn music_search(
    user_auth: UserAuth,
    libelle: String,
    db: DatabaseConn,
) -> Option<Json<JsonValue>> {
    let id = musique::table
        .filter(musique::libelle_musique.eq(libelle))
        .select(musique::id_musique)
        .first::<Option<i32>>(&db.0)
        .ok();

    match id {
        Some(id_found) => { let parsed_id: i32 = id_found.unwrap(); 
            return Some(Json(json!({"music": music_by_id(user_auth, parsed_id, db).unwrap().into_inner(), "id": parsed_id}))); 
        },
        _ => None,
    }
}

#[derive(Insertable)]
#[table_name = "musique_like"]
/// Structure permettant l'insertion d'une musique en base de données
pub struct InsertLikeMusic {
    /// ID de l'utilisateur qui aime la musique
    id_user: i32,
    /// ID de la musique concernée
    id_musique: i32,
}

#[post("/like/<id>")]
/// Route permettant d'ajouter un like sur une musique
pub fn music_like(user_auth: UserAuth, id: i32, db: DatabaseConn) -> Json<JsonValue> {
    if musique::table
        .filter(musique::id_musique.eq(id))
        .select(musique::id_musique)
        .first::<Option<i32>>(&db.0)
        .is_ok()
    {
        let insert_like_music = InsertLikeMusic {
            id_user: user_auth.user_id,
            id_musique: id,
        };
        let status = if diesel::insert_into(musique_like::table)
            .values(&insert_like_music)
            .execute(&db.0)
            .is_ok()
        {
            "success"
        } else {
            "error"
        };
        return Json(json!({ "status": status }));
    }
    Json(json!({"status": "error"}))
}

#[post("/unlike/<id>")]
/// Route permettant de retirer un like sur une musique
pub fn music_unlike(user_auth: UserAuth, id: i32, db: DatabaseConn) -> Json<JsonValue> {
    if diesel::delete(musique_like::table.filter(musique_like::id_user.eq(user_auth.user_id))).filter(musique_like::id_musique.eq(id)).execute(&db.0).is_ok() {
        return Json(json!({"status": "success"}));
    }
    Json(json!({"status": "error"}))
}