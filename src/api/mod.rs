/// Structure contenant les informations de libelle et l'ID
/// d'un élément concerné  
#[derive(Queryable, Clone, Serialize, Debug)]
pub struct APIRef {
    /// Libellé de l'élément
    pub libelle: String,
    /// ID de l'élément en base de données
    pub id: Option<i32>,
}

pub mod album;
pub mod artiste;
pub mod music;
