use crate::api::APIRef;

use tiamo_back_metadata::db::DatabaseConn;
use tiamo_back_metadata::schema::{artiste, artiste_like, musique};

use tiamo_back_metadata::user_auth::UserAuth;

use rocket_contrib::json::{Json, JsonValue};

use chrono::{NaiveDateTime, Utc};
use diesel::prelude::*;

#[derive(Queryable, Identifiable)]
#[table_name = "artiste"]
#[primary_key("id_artiste")]
pub struct Artist {
    id_artiste: Option<i32>,
    nom_artiste: String,
    date_naissance_artiste: Option<NaiveDateTime>,
    pays_artiste: Option<String>,
    groupe_musique_artiste: Option<String>,
}

#[derive(Insertable, AsChangeset)]
#[table_name = "artiste"]
pub struct InsertArtist {
    nom_artiste: String,
    date_naissance_artiste: Option<NaiveDateTime>,
    pays_artiste: Option<String>,
    groupe_musique_artiste: Option<String>,
}

#[derive(Serialize)]
pub struct APIArtist {
    nom_artiste: String,
    date_naissance_artiste: Option<String>,
    pays_artiste: Option<String>,
    groupe_musique_artiste: Option<String>,
    musiques: Vec<APIRef>,
}

#[derive(Deserialize)]
pub struct JSONArtist {
    nom_artiste: String,
    date_naissance_artiste: Option<String>,
    pays_artiste: Option<String>,
    groupe_musique_artiste: Option<String>,
}

impl Into<InsertArtist> for JSONArtist {
    fn into(self) -> InsertArtist {
        InsertArtist {
            nom_artiste: self.nom_artiste,
            pays_artiste: self.pays_artiste,
            groupe_musique_artiste: self.groupe_musique_artiste,
            date_naissance_artiste: self.date_naissance_artiste.map(|date| {
                NaiveDateTime::parse_from_str(&date, "%d-%m-%Y").unwrap_or(Utc::now().naive_local())
            }),
        }
    }
}

impl APIArtist {
    fn new(artist: Artist, musics: Vec<APIRef>) -> Self {
        APIArtist {
            date_naissance_artiste: artist
                .date_naissance_artiste
                .map(|date| date.format("%d-%m-%Y").to_string()),
            nom_artiste: artist.nom_artiste,
            pays_artiste: artist.pays_artiste,
            groupe_musique_artiste: artist.groupe_musique_artiste,
            musiques: musics,
        }
    }

    pub fn read(id: i32, db: &DatabaseConn) -> Option<APIArtist> {
        let artiste_res = artiste::table
            .filter(artiste::id_artiste.eq(id))
            .first::<Artist>(&db.0);

        if artiste_res.is_err() {
            return None;
        }
        let artiste = artiste_res.unwrap();

        let musics = musique::table
            .filter(musique::id_artiste.eq(id))
            .select((musique::libelle_musique, musique::id_musique))
            .load::<APIRef>(&db.0)
            .ok();

        match musics {
            Some(musics) => Some(APIArtist::new(artiste, musics)),
            _ => None,
        }
    }

    pub fn create(insert_artist: InsertArtist, db: &DatabaseConn) -> bool {
        diesel::insert_into(artiste::table)
            .values(&insert_artist)
            .execute(&db.0)
            .is_ok()
    }

    pub fn update(id: i32, insert_artist: InsertArtist, db: &DatabaseConn) -> bool {
        diesel::update(artiste::table)
            .filter(artiste::id_artiste.eq(id))
            .set(&insert_artist)
            .execute(&db.0)
            .is_ok()
    }
}

#[get("/<id>")]
pub fn artiste_by_id(_user_auth: UserAuth, id: i32, db: DatabaseConn) -> Option<Json<APIArtist>> {
    APIArtist::read(id, &db).map(|artist| Json(artist))
}

#[post("/", data = "<insert_artist>")]
pub fn artiste_insert(
    _user_auth: UserAuth,
    insert_artist: Json<JSONArtist>,
    db: DatabaseConn,
) -> Option<Json<APIArtist>> {
    let insert_artist: InsertArtist = insert_artist.into_inner().into();
    if APIArtist::create(insert_artist, &db) {
        let id = artiste::table
            .order(artiste::id_artiste.desc())
            .select(artiste::id_artiste)
            .first::<Option<i32>>(&db.0)
            .unwrap();
        return APIArtist::read(id.unwrap(), &db).map(|artist| Json(artist));
    }
    None
}

#[put("/<id>", data = "<insert_artist>")]
pub fn artiste_modif(
    _user_auth: UserAuth,
    insert_artist: Json<JSONArtist>,
    id: i32,
    db: DatabaseConn,
) -> Option<Json<APIArtist>> {
    let insert_artist: InsertArtist = insert_artist.into_inner().into();
    if APIArtist::update(id, insert_artist, &db) {
        let id = artiste::table
            .order(artiste::id_artiste.desc())
            .select(artiste::id_artiste)
            .first::<Option<i32>>(&db.0)
            .unwrap();
        return APIArtist::read(id.unwrap(), &db).map(|artist| Json(artist));
    }
    None
}

#[derive(Insertable)]
#[table_name = "artiste_like"]
pub struct InsertArtisteMusic {
    id_user: i32,
    id_artist: i32,
}

#[post("/like/<id>")]
pub fn artiste_like(user_auth: UserAuth, id: i32, db: DatabaseConn) -> Json<JsonValue> {
    if artiste::table
        .filter(artiste::id_artiste.eq(id))
        .select(artiste::id_artiste)
        .first::<Option<i32>>(&db.0)
        .is_ok()
    {
        let insert_like_artist = InsertArtisteMusic {
            id_user: user_auth.user_id,
            id_artist: id,
        };
        let status = if diesel::insert_into(artiste_like::table)
            .values(&insert_like_artist)
            .execute(&db.0)
            .is_ok()
        {
            "success"
        } else {
            "error"
        };
        return Json(json!({ "status": status }));
    }
    Json(json!({"status": "error"}))
}
