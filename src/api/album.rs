use crate::api::APIRef;
use chrono::{NaiveDateTime, Utc};

use tiamo_back_metadata::db::DatabaseConn;
use tiamo_back_metadata::models::Album;
use tiamo_back_metadata::schema::{album, album_like, musique};
use tiamo_back_metadata::user_auth::UserAuth;

use rocket_contrib::json::{Json, JsonValue};

use diesel::prelude::*;
use std::convert::Into;

#[derive(Serialize, Debug)]
/// Structure de données représentant un album distribué par l'API
pub struct APIAlbum {
    /// Identifiant de l'album en base
    id_album: Option<i32>,
    /// Nom de l'album
    nom_album: String,
    /// Date de parution de l'album, formatée en String
    date_parution_album: Option<String>,
    /// Liste de référence d'API vers les musiques de l'album
    musics: Vec<APIRef>,
}

#[derive(Insertable, AsChangeset, Identifiable)]
#[table_name = "album"]
#[primary_key("id_musique")]
pub struct InsertAlbum {
    /// ID en base de l'album
    id_album: Option<i32>,
    /// Nom de l'album
    nom_album: String,
    /// Date de parution de l'album
    date_parution_album: Option<NaiveDateTime>,
}

/// Structure récéptionnée en format JSON
#[derive(Deserialize, Debug)]
pub struct JSONAlbum {
    /// ID de l'album
    id: i32,
    /// Nom de l'album
    nom: String,
    /// Date au format JOUR-MOIS-ANNEE
    date_parution: Option<String>,
}

/// Permet de transformer un JSONAlbum en InsertAlbum
impl Into<InsertAlbum> for JSONAlbum {
    /// Effectue la transformation
    fn into(self) -> InsertAlbum {
        InsertAlbum {
            id_album: Some(self.id),
            nom_album: self.nom,
            date_parution_album: self.date_parution.map(|date| {
                NaiveDateTime::parse_from_str(&date, "%d-%m-%Y").unwrap_or(Utc::now().naive_local())
            }),
        }
    }
}

/// Implémentation d'un CRU pour APIAlbum
impl APIAlbum {
    /// Lecture d'un APIAlbum dont l'ID est spécifié depuis la base de données
    pub fn read(id: i32, db: &DatabaseConn) -> Option<APIAlbum> {
        let album = album::table
            .filter(album::id_album.eq(id))
            .first::<Album>(&db.0)
            .ok();

        if album.is_none() {
            return None;
        }

        let musics_db = album::table
            .filter(album::id_album.eq(id))
            .inner_join(musique::table)
            .select((musique::libelle_musique, musique::id_musique))
            .load::<APIRef>(&db.0)
            .ok();

        match (album, musics_db) {
            (Some(alb), Some(musics)) => Some(APIAlbum {
                id_album: alb.id_album,
                nom_album: alb.nom_album,
                date_parution_album: alb.date_parution_album.map(|date| date.to_string()),
                musics,
            }),
            _ => None,
        }
    }

    /// Création d'un APIAlbum dans la base de données
    pub fn create(insert_album: InsertAlbum, db: &DatabaseConn) -> bool {
        let existing_album = album::table
            .filter(album::nom_album.eq(insert_album.nom_album.clone()))
            .first::<Album>(&db.0);

        if existing_album.is_ok() {
            return false;
        }

        diesel::insert_into(album::table)
            .values(&insert_album)
            .execute(&db.0)
            .is_ok()
    }

    /// Mise à jour d'un APIAlbum en base de données
    pub fn update(id: i32, mut insert_album: InsertAlbum, db: &DatabaseConn) -> bool {
        insert_album.id_album = Some(id);
        let existing_album = album::table
            .filter(album::nom_album.eq(insert_album.nom_album.clone()))
            .first::<Album>(&db.0);

        if existing_album.is_err() {
            return false;
        }

        diesel::update(album::table)
            .set(&insert_album)
            .execute(&db.0)
            .is_ok()
    }
}

#[get("/<id>")]
/// Route permettant de récuperer la représentation JSON d'un APIAlbum spécifié par son ID 
pub fn album_by_id(_user_auth: UserAuth, id: i32, db: DatabaseConn) -> Option<Json<APIAlbum>> {
    APIAlbum::read(id, &db).map(|album| Json(album))
}

#[post("/", data = "<album>")]
/// Route permettant d'insérer un Album dans la base de données, via le JSON fourni en paramètre.
/// Le JSON suit la structure JSONAlbum
pub fn album_insert(
    _user_auth: UserAuth,
    album: Json<JSONAlbum>,
    db: DatabaseConn,
) -> Option<Json<APIAlbum>> {
    let id = album.id.clone();
    if APIAlbum::update(album.id, album.into_inner().into(), &db) {
        Some(Json(APIAlbum::read(id, &db).unwrap()))
    } else {
        None
    }
}

#[put("/<id>", data = "<album>")]
/// Route permettant de mettre à jour un Album spécifié par son ID en base de données.
/// Les informations seront modifiées suivant le JSON en paramètre, qui suit la structure JSONAlbum
pub fn album_update(
    _user_auth: UserAuth,
    id: i32,
    album: Json<JSONAlbum>,
    db: DatabaseConn,
) -> Option<Json<APIAlbum>> {
    if APIAlbum::update(id, album.into_inner().into(), &db) {
        Some(Json(APIAlbum::read(id, &db).unwrap()))
    } else {
        None
    }
}

#[derive(Insertable)]
#[table_name = "album_like"]
/// Structure permettant l'insertion du like d'un album en base de données 
pub struct InsertAlbumMusic {
    /// Identifiant de l'utilisateur qui aime l'album
    id_user: i32,
    /// Identifiant de l'album
    id_album: i32,
}

#[post("/like/<id>")]
/// Route permettant d'indiquer que l'utilisateur identifié par le Token apprécie
/// l'album dont l'ID est spécifié
pub fn album_like(user_auth: UserAuth, id: i32, db: DatabaseConn) -> Json<JsonValue> {
    if album::table
        .filter(album::id_album.eq(id))
        .select(album::id_album)
        .first::<Option<i32>>(&db.0)
        .is_ok()
    {
        let insert_like_album = InsertAlbumMusic {
            id_user: user_auth.user_id,
            id_album: id,
        };
        let status = if diesel::insert_into(album_like::table)
            .values(&insert_like_album)
            .execute(&db.0)
            .is_ok()
        {
            "success"
        } else {
            "error"
        };
        return Json(json!({ "status": status }));
    }
    Json(json!({"status": "error"}))
}
