use rocket_contrib::databases::diesel;

#[database("tiamo_db")]
/// Structure stockant la connection à une base de données SQLite
pub struct DatabaseConn(diesel::SqliteConnection);
