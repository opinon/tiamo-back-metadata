use crate::db::DatabaseConn;
use crate::models::Token;
use chrono::NaiveDateTime;
use rocket::http::Status;
use rocket::request::FromRequest;
use rocket::request::Outcome;
use rocket::Request;

use crate::schema::{token, user};
use chrono::Utc;
use diesel::prelude::*;
use time::Duration;

/// Représente un utilisateur authentifié via un Token.
/// Doit être utilisé comme un garde de requête.
pub struct UserAuth {
    /// ID de l'utilisateur authentifié
    pub user_id: i32,
}

/// Implémentation du trait UserAuth permettant de
impl<'a, 'r> FromRequest<'a, 'r> for UserAuth {
    type Error = &'a str;

    fn from_request(request: &'a Request<'r>) -> Outcome<UserAuth, Self::Error> {
        // Récupère le Token d'authentification
        let req_header = request.headers().get_one("Authorization");
        if req_header.is_none() {
            return Outcome::Failure((Status::BadRequest, "Pas de token d'authentification fourni"));
        }
        let auth_token: String = req_header.unwrap().into();

        // Récupère une connection à la base de données
        let db = request.guard::<DatabaseConn>();
        match db {
            Outcome::Failure(_) | Outcome::Forward(_) => {
                return Outcome::Failure((
                    Status::InternalServerError,
                    "Problème de connection à la base de données",
                ));
            }
            _ => (),
        };

        let db: DatabaseConn = db.unwrap();
        let res = token::table
            .filter(token::content.eq(auth_token))
            .inner_join(user::table)
            .select((token::all_columns, user::id_user))
            .load::<(Token, i32)>(&db.0)
            .ok();

        match res {
            Some(result) if result.len() == 1 => {
                let user_token = &result[0];
                let now: NaiveDateTime = Utc::now().naive_local();
                let limit = user_token.0.date_creation
                    + Duration::days(user_token.0.validity_period_days as i64);
                if limit > now {
                    Outcome::Success(UserAuth {
                        user_id: user_token.1,
                    })
                } else {
                    Outcome::Failure((Status::BadRequest, "Outdated token"))
                }
            }
            _ => Outcome::Failure((Status::NotFound, "No user matching the token")),
        }
    }
}
