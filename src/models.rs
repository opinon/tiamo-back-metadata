use crate::schema::{album, friend_with, token, user};
use chrono::NaiveDateTime;

#[derive(Insertable)]
#[table_name = "user"]
pub struct InsertUser {
    /// Identifiant du token unique associé à l'utilisateur
    pub id_token: Option<i32>,
    /// Date de création de l'utilisateur
    pub date_creation: NaiveDateTime,
    /// Libelle de l'utilisateur (son pseudo)
    pub libelle_user: String,
    /// Mot de passe de l'utilisateur
    pub mdp_user: String,
}

#[derive(Queryable, Insertable, PartialEq, Debug)]
#[table_name = "token"]
/// Représente un Token utilisateur dans la base de données
pub struct Token {
    /// Identifiant du Token
    pub id_token: i32,
    /// Contenu du Token (identifiant textuel)
    pub content: String,
    /// Date de création du token
    pub date_creation: NaiveDateTime,
    /// Validité en jour du token (avant expiration)
    pub validity_period_days: i32,
}

#[derive(Insertable)]
#[table_name = "token"]
/// Représente un token à insérer dans la base de données
pub struct InsertToken {
    /// Contenu du token
    pub content: String,
    /// Date de création du token
    pub date_creation: NaiveDateTime,
    /// Durée de validité du token
    pub validity_period_days: i32,
}

#[derive(Insertable, PartialEq, Debug)]
#[table_name = "friend_with"]
/// Représente un lien d'amitié (bi-directionnel) entre deux
/// utilisateurs
pub struct Friendship {
    /// Utilisateur 1
    id_user_1: i32,
    /// Utilisateur 2
    id_user_2: i32,
    /// Date à laquelle l'amitié a été déclarée
    date_association: NaiveDateTime,
}

#[derive(Queryable, Identifiable)]
#[table_name = "album"]
#[primary_key("id_album")]
/// Représente un Album stocké dans la base de données
pub struct Album {
    /// Identifiant de l'album
    pub id_album: Option<i32>,
    /// Nom de l'album en base
    pub nom_album: String,
    /// Date de parution de l'album
    pub date_parution_album: Option<NaiveDateTime>,
}
