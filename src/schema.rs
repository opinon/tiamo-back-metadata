table! {
    album (id_album) {
        id_album -> Nullable<Integer>,
        nom_album -> Text,
        date_parution_album -> Nullable<Timestamp>,
    }
}

table! {
    album_like (id_album_like) {
        id_album_like -> Nullable<Integer>,
        id_user -> Integer,
        id_album -> Integer,
    }
}

table! {
    artiste (id_artiste) {
        id_artiste -> Nullable<Integer>,
        nom_artiste -> Text,
        date_naissance_artiste -> Nullable<Timestamp>,
        pays_artiste -> Nullable<Text>,
        groupe_musique_artiste -> Nullable<Text>,
    }
}

table! {
    artiste_like (id_artist_like) {
        id_artist_like -> Nullable<Integer>,
        id_user -> Integer,
        id_artist -> Integer,
    }
}

table! {
    friend_with (id_user_1, id_user_2) {
        id_user_1 -> Integer,
        id_user_2 -> Integer,
        date_association -> Timestamp,
    }
}

table! {
    genre (id_genre) {
        id_genre -> Nullable<Integer>,
        libelle_genre -> Text,
    }
}

table! {
    genre_like (id_genre_like) {
        id_genre_like -> Nullable<Integer>,
        id_user -> Nullable<Integer>,
        id_genre -> Nullable<Integer>,
    }
}

table! {
    metadata_custom (id_metadata) {
        id_metadata -> Nullable<Integer>,
        type_metadata -> Text,
        libelle_metadata -> Text,
    }
}

table! {
    musique (id_musique) {
        id_musique -> Nullable<Integer>,
        libelle_musique -> Text,
        miniature_path -> Nullable<Text>,
        id_artiste -> Nullable<Integer>,
        id_album -> Nullable<Integer>,
    }
}

table! {
    musique_collection (id_musique, id_collection) {
        id_musique -> Nullable<Integer>,
        id_collection -> Nullable<Integer>,
    }
}

table! {
    musique_genre (id_musique, id_genre) {
        id_musique -> Nullable<Integer>,
        id_genre -> Nullable<Integer>,
    }
}

table! {
    musique_like (id_musique_like) {
        id_musique_like -> Nullable<Integer>,
        id_user -> Integer,
        id_musique -> Integer,
    }
}

table! {
    tiamo_collection (id_collection) {
        id_collection -> Nullable<Integer>,
        libelle_collection -> Text,
        id_type_collection -> Integer,
    }
}

table! {
    token (id_token) {
        id_token -> Integer,
        content -> Text,
        date_creation -> Timestamp,
        validity_period_days -> Integer,
    }
}

table! {
    type_collection (id_type_collection) {
        id_type_collection -> Nullable<Integer>,
        libelle_type_collection -> Text,
    }
}

table! {
    user (id_user) {
        id_user -> Integer,
        id_token -> Nullable<Integer>,
        date_creation -> Timestamp,
        libelle_user -> Text,
        mdp_user -> Text,
    }
}

joinable!(album_like -> album (id_album));
joinable!(album_like -> user (id_user));
joinable!(artiste_like -> artiste (id_artist));
joinable!(artiste_like -> user (id_user));
joinable!(friend_with -> user (id_user_1));
joinable!(genre_like -> genre (id_genre));
joinable!(genre_like -> user (id_user));
joinable!(musique -> album (id_album));
joinable!(musique -> artiste (id_artiste));
joinable!(musique_collection -> musique (id_musique));
joinable!(musique_collection -> tiamo_collection (id_collection));
joinable!(musique_genre -> genre (id_genre));
joinable!(musique_genre -> musique (id_musique));
joinable!(musique_like -> musique (id_musique));
joinable!(musique_like -> user (id_user));
joinable!(tiamo_collection -> type_collection (id_type_collection));
joinable!(user -> token (id_token));

allow_tables_to_appear_in_same_query!(
    album,
    album_like,
    artiste,
    artiste_like,
    friend_with,
    genre,
    genre_like,
    metadata_custom,
    musique,
    musique_collection,
    musique_genre,
    musique_like,
    tiamo_collection,
    token,
    type_collection,
    user,
);
