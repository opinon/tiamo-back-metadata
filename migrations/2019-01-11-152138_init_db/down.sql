-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS album;
DROP TABLE IF EXISTS album_like;
DROP TABLE IF EXISTS artiste;
DROP TABLE IF EXISTS artiste_like;
DROP TABLE IF EXISTS tiamo_collection;
DROP TABLE IF EXISTS friend_with;
DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS genre_like;
DROP TABLE IF EXISTS metadata_custom;
DROP TABLE IF EXISTS musique;
DROP TABLE IF EXISTS musique_collection;
DROP TABLE IF EXISTS musique_genre;
DROP TABLE IF EXISTS musique_like;
DROP TABLE IF EXISTS repertoire_de_recherche;
DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS type_collection;
DROP TABLE IF EXISTS user;