-- Table : album
CREATE TABLE album (
    id_album            INTEGER PRIMARY KEY AUTOINCREMENT,
    nom_album           TEXT    NOT NULL,
    date_parution_album DATETIME
);


-- Table : album_like
CREATE TABLE album_like (
    id_album_like INTEGER PRIMARY KEY AUTOINCREMENT,
    id_user       INTEGER REFERENCES user (id_user) NOT NULL,
    id_album      INTEGER REFERENCES album (id_album) NOT NULL 
);

-- Table : artiste
CREATE TABLE artiste ( 
    id_artiste  INTEGER PRIMARY KEY AUTOINCREMENT, 
    nom_artiste TEXT NOT NULL, 
    date_naissance_artiste DATETIME, 
    pays_artiste TEXT, 
    groupe_musique_artiste TEXT
);

-- Table : artiste_like
CREATE TABLE artiste_like (
    id_artist_like INTEGER PRIMARY KEY AUTOINCREMENT,
    id_user        INTEGER REFERENCES user (id_user) NOT NULL,
    id_artist      INTEGER REFERENCES artiste (id_artiste) NOT NULL 
);


-- Table : tiamo_collection
CREATE TABLE tiamo_collection (
    id_collection      INTEGER PRIMARY KEY AUTOINCREMENT,
    libelle_collection TEXT    NOT NULL,
    id_type_collection INTEGER NOT NULL,
    FOREIGN KEY (
        id_type_collection
    )
    REFERENCES type_collection (id_type_collection) ON DELETE CASCADE
                                                    ON UPDATE CASCADE
);

-- Table : user
CREATE TABLE user (
    id_user       INTEGER  PRIMARY KEY AUTOINCREMENT
                           NOT NULL,
    id_token      INTEGER  CONSTRAINT fk_id_token_user_token REFERENCES token (id_token),
    date_creation DATETIME NOT NULL,
    libelle_user  TEXT     NOT NULL,
    mdp_user      TEXT     NOT NULL
);
 
 
-- Table : friend_with
CREATE TABLE friend_with (
    id_user_1        INTEGER  REFERENCES user (id_user) 
                              NOT NULL,
    id_user_2        INTEGER  NOT NULL,
    date_association DATETIME NOT NULL,
    PRIMARY KEY (id_user_1, id_user_2)
);

-- Table : genre
CREATE TABLE genre (
    id_genre      INTEGER PRIMARY KEY AUTOINCREMENT,
    libelle_genre TEXT    NOT NULL
);

-- Table : genre_like
CREATE TABLE genre_like (
    id_genre_like INTEGER PRIMARY KEY AUTOINCREMENT,
    id_user       INTEGER REFERENCES user (id_user),
    id_genre      INTEGER REFERENCES genre (id_genre) 
);


-- Table : metadata_custom
CREATE TABLE metadata_custom (
    id_metadata      INTEGER PRIMARY KEY AUTOINCREMENT,
    type_metadata    TEXT    NOT NULL,
    libelle_metadata TEXT    NOT NULL
);


-- Table : musique
CREATE TABLE musique (
    id_musique         INTEGER PRIMARY KEY AUTOINCREMENT,
    libelle_musique    TEXT    NOT NULL,
    miniature_path     TEXT,
    id_artiste         INTEGER,
    id_album           INTEGER,
    FOREIGN KEY (
        id_artiste
    )
    REFERENCES artiste (id_artiste) ON DELETE CASCADE
                                    ON UPDATE CASCADE,
    FOREIGN KEY (
        id_album
    )
    REFERENCES album (id_album) ON DELETE CASCADE
                                ON UPDATE CASCADE
);

-- Table : musique_collection
CREATE TABLE musique_collection (
    id_musique    INTEGER REFERENCES musique(id_musique),
    id_collection INTEGER REFERENCES tiamo_collection(id_collection),
    PRIMARY KEY (id_musique, id_collection)
);

-- Table : musique_genre
CREATE TABLE musique_genre (
    id_musique INTEGER REFERENCES musique (id_musique),
    id_genre   INTEGER REFERENCES genre (id_genre),
    PRIMARY KEY (id_musique, id_genre)
);


-- Table : musique_like
CREATE TABLE musique_like (
    id_musique_like INTEGER PRIMARY KEY AUTOINCREMENT,
    id_user         INTEGER REFERENCES user (id_user) NOT NULL,
    id_musique      INTEGER REFERENCES musique (id_musique) NOT NULL 
);

-- Table : token
CREATE TABLE token (
    id_token             INTEGER  PRIMARY KEY AUTOINCREMENT
                                  NOT NULL,
    content              TEXT     NOT NULL,
    date_creation        DATETIME NOT NULL,
    validity_period_days INTEGER  NOT NULL
);

-- Table : type_collection
CREATE TABLE type_collection (
    id_type_collection      INTEGER PRIMARY KEY AUTOINCREMENT,
    libelle_type_collection TEXT    NOT NULL
);