# tiamo-back-metadata

TIAMO Back-End - Gestion de metadata

# Installer et lancer le projet

* [Installer Rust](https://rustup.rs/)
* [Installer SQLite](https://www.tutorialspoint.com/sqlite/sqlite_installation.htm) - et ses libraires de développement selon le système
* Installer diesel_cli avec la feature SQLite: `cargo install diesel_cli --no-default-features --features sqlite`
* Setup la base de données : `diesel setup` puis `diesel migration run`
* Compiler le programme : `cargo build --release`
* L'éxécutable se trouvera dans /target/release/tiamo-back-metadata
* Le port 8000 sera utilisé par l'application pour faire tourner un serveur HTTP
* Documentation: `cargo doc` pour générer la documentation, celle-ci est partagée entre les crates: 
    * tiamo_back_metadata  
    * server

# API

## Gestion des utilisateurs 

Méthode | Endpoint | Params | Description | Retour 
--- | --- | --- | --- | ---
POST | /user | Header HTTP: User et Pass | Crée un nouvel utilisateur. Retourne un JSON (cf. src/user_api.rs) | ``` { "status": "error ou success", "reason si error, ou token": "" } ```
GET | /user/token | Header HTTP: User et Pass | Crée un token pour l'utilisateur donné | Erreur 404 ou ```{ "token" : "String token" }```
PUT | /user | Header HTTP: Authorization // body contient un JSON ```{ user: String, pass: String }``` | Change les informations de l'utilisateur identifié par le Token | Erreur 404 ou ```{ "date_creation" : "JOUR-MOIS-ANNEE HEURE:MINUTE", "libelle_user": "pseudo", music_liked: [ { "libelle": "libelle_musique", id: x }, ... ] }```
POST | /user/befriend/**libelle_user** | Header HTTP: Authorization // libelle_user: String | Ajoute une relation d'amitié avec l'utilisateur dont le pseudo est donné. Retourne: 200 avec nom de l'utilisateur ajouté, 404 si utilisateur pas trouvé, 400 si erreur d'insertion | Erreur 404 si pas d'user avec ce nom, 400 si déjà ami, sinon, retourne le nom de l'utilisateur sous forme de String 
GET | /user/profile | Header HTTP: Authorization | Retourne une représentation JSON de l'utilisateur identifié par le token | ```{ "date_creation" : "JOUR-MOIS-ANNEE HEURE:MINUTE", "libelle_user": "pseudo", music_liked: [ { "libelle": "libelle_musique", id: x }, ... ] }```
GET | /user/friends | Header HTTP: Authorization | Retourne un JSON contenant un tableau de références d'API vers les utilisateurs que l'utilisateur authentifié suit | Erreur 404 ou ```{ [ { "libelle": "nom_user", id: x }, ... ] }``` 
GET | /user/profile/**id** | Header HTTP: Authorization | Retourne la fiche descriptive d'un utilisateur dont l'ID est donné en paramètre | Erreur 404 ou ```{ "date_creation" : "JOUR-MOIS-ANNEE HEURE:MINUTE", "libelle_user": "pseudo", music_liked: [ { "libelle": "libelle_musique", id: x }, ... ] }```

## Gestion des musiques

Méthode | Endpoint | Params | Description | Retour 
--- | --- | --- | --- | ---
GET | /music/**id** | Header HTTP: Authorization // id: integer | Récupère la représentation d'une musique au format JSON (cf. src/api/music.rs), ou une erreur 404 si aucune musique n'est trouvée | Erreur 404 ou ```{ "titre": "", "artiste": { "libelle": "", "id": x }, "album": { "libelle": "", "id": x }, genres: [ {"libelle": "", "id": x }] }``` 
GET | /music/page/**id_page** | Header HTTP: Authorization // id_page: Integer | Récupère la représentation JSON d'un tableau de musique (cf. src/api/music.rs) | ``` { [ { "titre": "", "artiste": { "libelle": "", "id": x }, "album": { "libelle": "", "id": x }, genres: [ {"libelle": "", "id": x }] }, ... ]} ```
POST | /music/**libelle** | Header HTTP: Authorization // libelle: String | Insère une nouvelle musique avec le libelle donné en paramètre de l'URL. Retourne la représentation JSON de la musique insérée | Erreur 404 ou ```{ "titre": "", "artiste": { "libelle": "", "id": x }, "album": { "libelle": "", "id": x }, genres: [ {"libelle": "", "id": x }] }``` 
PUT | /music/**id** | Header HTTP: Authorization // id: Integer // body contient un JSON (cf. src/api/music.rs) | Modifie les données de la musique par le fichier JSON passé en paramètre. Retourne la représentation JSON de la musique, ou une erreur 400 ou 404 si le JSON est malformé | Erreur 404 ou ```{ "titre": "", "artiste": { "libelle": "", "id": x }, "album": { "libelle": "", "id": x }, genres: [ {"libelle": "", "id": x }] }```
POST | /music/like/**id** | Header HTTP: Authorization // id: Integer | Permet d'ajouter un like sur une musique pour l'utilisateur authentifié sur la musique spécifié | ```{ "status" : "success ou error"}```
POST | /music/unlike/**id** | Header HTTP: Authorization // id: Integer | Permet de retirer un like sur une musique pour l'utilisateur authentifié sur la musique spécifié | ```{ "status" : "success ou error"}```
GET | /music/search/**libelle** | Header HTTP: Authorization // libelle: String (nom de la musique) | Recherche la musique dont le libelle est donné en paramètre. Retourne une représentation JSON : ```{id: Integer, music: APIMusic}```| Erreur 404 ou ```{ "titre": "", "artiste": { "libelle": "", "id": x }, "album": { "libelle": "", "id": x }, genres: [ {"libelle": "", "id": x }] }```

## Gestion des albums

Méthode | Endpoint | Params | Description | Retour 
--- | --- | --- | --- | ---
GET | /album/**id** | Header HTTP: Authorization // id: Integer | Donne les informations au format JSON de l'album spécifié par son ID | Erreur 404 ou ```{ "id_album": x, "nom_album": "nom", "date_parution_album": "JOUR-MOIS-ANNEE", "musics": [{"libelle": "nom", "id": x}]}```
POST | /album | Header HTTP: Authorization // body contient un JSON (cf. src/api/album.rs) | Crée un nouvel album avec les informations données dans le JSON | Erreur 404 ou ```{ "id_album": x, "nom_album": "nom", "date_parution_album": "JOUR-MOIS-ANNEE", "musics": [{"libelle": "nom", "id": x}]}```
POST | /album/like/**id** | Header HTTP: Authorization // id: Integer | Permet d'indiquer que l'utilisateur authentifié par le token aime la album dont l'ID est donné dans l'URL | ```{"status": "success ou error" }``` 
PUT | /album/**id** | Header HTTP: Authorization // id: Integer // body contient un JSON (cf. src/api/album.rs) | Permet de modifier les données de l'album suivant le JSON passé en paramètre | Erreur 404 ou ```{ "id_album": x, "nom_album": "nom", "date_parution_album": "JOUR-MOIS-ANNEE", "musics": [{"libelle": "nom", "id": x}]}```


## Gestion des artistes

Méthode | Endpoint | Params | Description | Retour 
--- | --- | --- | --- | ---
GET | /artiste/**id** | Header HTTP: Authorization // id: Integer | Donne les informations au format JSON de l'artiste spécifié par son ID | Erreur 404 ou ```{ "nom_artiste": "nom", "date_naissance_artiste": "JOUR-MOIS-ANNEE", "pays_artiste": "pays", "groupe_musique_artiste": "groupe", musiques: [ { "libelle": "nom", "id": x }, ...]}```
POST | /artiste | Header HTTP: Authorization // body contient un JSON (cf. src/api/artiste.rs) | Crée un nouvel album avec les informations données dans le JSON | Erreur 404 ou ```{ "nom_artiste": "nom", "date_naissance_artiste": "JOUR-MOIS-ANNEE", "pays_artiste": "pays", "groupe_musique_artiste": "groupe", musiques: [ { "libelle": "nom", "id": x }, ...]}```
POST | /artiste/like/**id** | Header HTTP: Authorization // id: Integer | Permet d'indiquer que l'utilisateur authentifié par le token aime l'artiste dont l'ID est donné dans l'URL | ```{"status": "success ou error" }``` 
PUT | /artiste/**id** | Header HTTP: Authorization // id: Integer // body contient un JSON (cf. src/api/artiste.rs) | Permet de modifier les données de l'album suivant le JSON passé en paramètre | Erreur 404 ou ```{ "nom_artiste": "nom", "date_naissance_artiste": "JOUR-MOIS-ANNEE", "pays_artiste": "pays", "groupe_musique_artiste": "groupe", musiques: [ { "libelle": "nom", "id": x }, ...]}```

